<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4Users\Tests\Helpers;

use Bittacora\Bpanel4Users\Models\User;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Foundation\Testing\Concerns\InteractsWithAuthentication;
use Illuminate\Foundation\Testing\WithFaker;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
use Tests\TestCase;

final class AdminHelper
{
    use WithFaker;
    use InteractsWithAuthentication;

    private Application $app;

    public function __construct()
    {
        $this->app = resolve('app');
        $this->setUpFaker();
    }

    public function actingAsAdmin(): User
    {
        $admin = $this->createAdmin();
        $this->actingAs($admin);
        return $admin;
    }

    public function actingAsAdminWithPermissions(array $permissions): User
    {
        $admin = $this->actingAsAdmin();

        foreach ($permissions as $permission) {
            $admin->givePermissionTo(Permission::firstOrCreate(['name' => $permission]));
        }

        return $admin;
    }

    public function createAdminRole(): Role
    {
        return Role::findOrCreate('admin', 'web');
    }

    public function createAdmin(): User
    {
        $user = new User();
        $user->setName($this->faker->name());
        $user->setEmail($this->faker->email());
        $user->assignRole($this->createAdminRole());
        $user->setPassword('password');
        $user->setActive(1);
        $user->save();

        return $user;
    }
}
