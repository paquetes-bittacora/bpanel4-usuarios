<?php

declare(strict_types=1);

return [
    'user' => 'Usuarios',
    'index' => 'Listar usuarios',
    'edit' => 'Editar usuario',
    'create' => 'Crear usuario',
    'show' => 'Mostrar usuario',
    'destroy' => 'Borrar usuario',
    'bulkdelete' => 'Borrar en lote'
];
