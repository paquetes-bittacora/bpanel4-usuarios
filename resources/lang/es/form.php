<?php

return [
    'name' => 'Nombre',
    'email' => 'Correo electrónico',
    'active' => 'Activo',
    'add' => 'Alta de usuario',
    'edit' => 'Edición de usuario',
    'password' => 'Contraseña',
    'repeat_password' => 'Repetir contraseña',
    'assign_roles_to_user' => 'Asignar roles a usuario',
    'change_password' => 'Cambiar contraseña (marca esta casilla para cambiar la contraseña del usuario)'
];
