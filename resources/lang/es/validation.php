<?php

return [
    'required_field' => 'Este campo es obligatorio',
    'email_attribute' => 'correo electrónico',
    'password_attribute' => 'contraseña',
    'repeat_password_attribute' => 'repetir contraseña',
    'name_attribute' => 'nombre',
    'required_message' => 'Este campo es obligatorio',
    'max_message' => 'Este campo solo puede tener un máximo de :max caracteres',
    'name_unique_message' => 'Ya existe un usuario con el nombre: :input',
    'email_unique_message' => 'Ya existe un usuario con el correo electrónico: :input',
    'email_email_message' => 'El valor :input no es un formato de correo electrónico válido',
    'password_same_field' => 'Los campos "contraseña" y "repetir contraseña" deben coincidir'
];
