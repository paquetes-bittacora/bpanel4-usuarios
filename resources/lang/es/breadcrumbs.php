<?php

return [
    'user' => 'Usuarios',
    'create' => 'Añadir',
    'edit' => 'Editar',
    'index' => 'Listar',
    'show' => 'Mostrar'
];
