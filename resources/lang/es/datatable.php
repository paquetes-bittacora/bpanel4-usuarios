<?php

return [
    'index' => 'Listado de usuarios',
    'name' => 'Nombre',
    'email' => 'Correo electrónico',
    'active' => 'Activo',
    'actions' => 'Acciones',
    'download' => 'Descargar',
    'print' => 'Imprimir',
    'the_user' => 'el usuario',
    'user_info' => 'Información del usuario',
    'yes' => 'Sí',
    'no' => 'No',
    'created_date' => 'Fecha de alta',
    'updated_date' => 'Última edición'
];
