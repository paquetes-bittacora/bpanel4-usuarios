@extends('bpanel4::layouts.bpanel-app')

@section('title', 'Usuarios')

@section('content')

    <div class="card bcard">
        <div class="card-header bgc-primary-d1 text-white border-0">
            <h4 class="text-120 mb-0">
                <span class="text-90">{{ __('user::datatable.index') }}</span>
            </h4>
        </div>
        <div class="card-body">
            @livewire('user::user-datatable', [], key('user-datatable'))
        </div>
    </div>


@endsection
