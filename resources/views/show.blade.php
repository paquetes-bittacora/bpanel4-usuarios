@extends('bpanel4::layouts.bpanel-app')

@section('title', 'Datos de usuario: ' . $user->name)

@section('content')

    <div class="card-header bgc-primary-d1 text-white border-0">
        <h4 class="text-120 mb-0">
            <span class="text-90">{{ __('user::datatable.user_info') }}</span>
        </h4>
    </div>
    <table class="table">
        <tbody>
            <tr>
                <td class="col-sm-2"><strong>{{ __('user::datatable.name') }}</strong></td>
                <td>
                    {{ $user->name }}
                </td>
            </tr>
            <tr>
                <td class="col-sm-2"><strong>{{ __('user::datatable.email') }}</strong></td>
                <td>
                    {{ $user->email }}
                </td>
            </tr>
            <tr>
                <td class="col-sm-2"><strong>{{ __('user::datatable.active') }}</strong></td>
                <td>
                    @if($user->active) {{ __('user::datatable.yes') }} @else {{ __('user::datatable.no') }} @endif
                </td>
            </tr>
            <tr>
                <td class="col-sm-2"><strong>{{ __('user::datatable.created_date') }}</strong></td>
                <td>
                    {{ $user->created_at->format('d/m/Y H:i:s') }} @if(!is_null($user->created_by)) - {{ \Bittacora\Bpanel4Users\UserFacade::getNameById($user->created_by) }} @endif
                </td>
            </tr>
            <tr>
                <td class="col-sm-2"><strong>{{ __('user::datatable.updated_date') }}</strong></td>
                <td>
                    {{ $user->updated_at->format('d/m/Y H:i:s') }} @if(!is_null($user->updated_by)) - {{ \Bittacora\Bpanel4Users\UserFacade::getNameById($user->updated_by) }} @endif
                </td>
            </tr>
            <tr>
                <td class="col-sm-2"><strong>Roles</strong></td>
                <td>
                    @foreach($user->roles as $role)
                        {{$role->name}} @if(!$loop->last) / @endif
                    @endforeach
                </td>
            </tr>
        </tbody>
    </table>

@endsection
