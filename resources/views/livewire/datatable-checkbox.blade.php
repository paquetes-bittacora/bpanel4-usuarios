<div class="form-check form-check-inline text-center mr-0">
    <input class="form-check-input ace-switch mr-0"
           type="checkbox"
           @if($multiple) name="{{$name}}[]" @else name="{{$name}}" @endif
           id="{{$idField}}"
           wire:click="toggle({{$idField}})"
           @if($value==1) checked @endif
    >
    @if(!is_null($labelText))
        <label class="form-check-label" for="{{$idField}}">{{ $labelText }}</label>
    @endif
</div>
