<td>
    <div class="text-center">
        {{$row->name}}
    </div>
</td>
<td>
    <div class="text-center">
        {{$row->email}}
    </div>
</td>
<td>
    <div class="text-center">
        {{\Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $row->created_at)->format('d/m/Y H:i')}}
    </div>
</td>
<td>
    @livewire('utils::datatable-default', ['fieldName' => 'active', 'model' => $row, 'value' => $row->active, 'size' => 'xxs'], key('active-user-'.$row->id))
</td>
<td>
    <div class="text-center">
        @livewire('utils::datatable-action-buttons', ['actions' => ["edit", "delete"], 'scope' => 'user', 'model' => $row, 'permission' => ['edit', 'delete'], 'id' => $row->id, 'message' => 'el usuario?'], key('user-buttons-'.$row->id))
    </div>
</td>
