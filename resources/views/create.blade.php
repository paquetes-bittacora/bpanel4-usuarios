@extends('bpanel4::layouts.bpanel-app')

@section('title', 'Alta de usuario')

@section('content')
    <div class="card bcard">
        <div class="card-header bgc-primary-d1 text-white border-0">
            <h4 class="text-120 mb-0">
                <span class="text-90">{{ __('user::form.add') }}</span>
            </h4>
        </div>
        <form class="mt-lg-3" autocomplete="off" method="post" action="{{route('user.store')}}">
            @csrf
            @livewire('form::input-text', ['name' => 'name', 'labelText' => __('user::form.name'), 'labelWidth' => 3, 'required' => true])
            @livewire('form::input-text', ['name' => 'email', 'labelText' => __('user::form.email'), 'labelWidth' => 3, 'required' => true])
            @livewire('form::input-password', ['name' => 'password', 'labelText' => __('user::form.password'), 'labelWidth' => 3, 'required' => true, 'note' => 'Debe contener entre 8 y 16 caracteres'])
            @livewire('form::input-password', ['name' => 'repeatPassword', 'labelText' => __('user::form.repeat_password'), 'labelWidth' => 3, 'required' => true])
            @livewire('form::input-checkbox', ['name' => 'active', 'labelText' => __('user::form.active'), 'bpanelForm' => true, 'checked' => true])

            @if(!empty($roles))
                @livewire('form::dual-list-box', ['name' => 'roles[]', 'idField' => 'roleSelect', 'labelText' => __('user::form.assign_roles_to_user'), 'allValues' => $roles, 'height' => '232px'])
            @endif


            <div class="col-12 mt-5 border-t-1 bgc-secondary-l4 brc-secondary-l2 py-35 d-flex justify-content-center">
                @livewire('form::save-button',['theme'=>'save'])
                @livewire('form::save-button',['theme'=>'reset'])
            </div>
        </form>
    </div>

@endsection
