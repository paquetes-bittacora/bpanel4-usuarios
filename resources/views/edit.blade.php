@extends('bpanel4::layouts.bpanel-app')

@section('title', 'Edición de usuario: ' . $user->name)

@section('content')
    <div class="card bcard">
        <div class="card-header bgc-primary-d1 text-white border-0">
            <h4 class="text-120 mb-0">
                <span class="text-90">{{ __('user::form.edit') }}</span>
            </h4>
        </div>
        <form class="mt-lg-3" autocomplete="off" method="post" action="{{route('user.update', $user)}}">
            @method('PUT')
            @csrf
            @livewire('form::input-text', ['name' => 'name', 'labelText' => __('user::form.name'), 'labelWidth' => 3, 'required' => true, 'value' => $user->name])
            @livewire('form::input-text', ['name' => 'email', 'labelText' => __('user::form.email'), 'labelWidth' => 3, 'required' => true, 'value' => $user->email])
            @livewire('form::input-password', ['name' => 'password', 'labelText' => __('user::form.password'), 'labelWidth' => 3])
            @livewire('form::input-password', ['name' => 'repeatPassword', 'labelText' => __('user::form.repeat_password'), 'labelWidth' => 3])
            @livewire('form::input-checkbox', ['name' => 'active', 'labelText' => __('user::form.active'), 'bpanelForm' => true, 'checked' => $user->active])
            @livewire('form::input-checkbox', ['name' => 'changePassword', 'labelText' => __('user::form.change_password'), 'bpanelForm' => true, 'checked' => false])

            @if(!empty($roles))
                @livewire('form::dual-list-box', ['name' => 'roles[]', 'idField' => 'roleSelect', 'labelText' => __('user::form.assign_roles_to_user'),
                'allValues' => $roles, 'height' => '232px', 'selectedValues' => $userRoles])
            @endif

            @livewire('utils::created-updated-info', ['model' => $user])

            <div class="col-12 mt-5 border-t-1 bgc-secondary-l4 brc-secondary-l2 py-35 d-flex justify-content-center">
                @livewire('form::save-button',['theme'=>'update'])
                @livewire('form::save-button',['theme'=>'reset'])
            </div>
        </form>
    </div>

@endsection
