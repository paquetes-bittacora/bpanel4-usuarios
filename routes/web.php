<?php

use Bittacora\Bpanel4Users\Http\Controllers\UserController;
use Illuminate\Support\Facades\Route;

Route::prefix('bpanel')->middleware(['web', 'auth', 'admin-menu'])->name('user.')->group(function(){
    Route::get('/user', [UserController::class, 'index'])->name('index');
    Route::get('/user/create', [UserController::class, 'create'])->name('create');
    Route::get('/user/{user}/show', [UserController::class, 'show'])->name('show');
    Route::get('/user/{user}/edit', [UserController::class, 'edit'])->name('edit');
    Route::post('/user/store', [UserController::class, 'store'])->name('store');
    Route::put('/user/{user}', [UserController::class, 'update'])->name('update');
    Route::delete('/user/{user}', [UserController::class, 'destroy'])->name('destroy');
    Route::post('/user/bulkdelete', [UserController::class, 'bulkDelete'])->name('bulkdelete');
});
