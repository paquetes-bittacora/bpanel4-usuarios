<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4Users\Middleware;

use Closure;
use Illuminate\Http\Request;

final class LogoutInactiveUserMiddleware
{
    public function handle(Request $request, Closure $next) {
        $user = auth()->user();

        if (null === $user) {
            return $next($request);
        }

        if ($user->active === 0) {
            auth()->logout();
            return redirect('/');
        }

        return $next($request);
    }
}