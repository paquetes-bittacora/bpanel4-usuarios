<?php

namespace Bittacora\Bpanel4Users\Http\Controllers;

use App\Http\Controllers\Controller;
use Bittacora\Bpanel4Users\DataTables\UserDatatable;
use Bittacora\Bpanel4Users\Http\Requests\StoreUserRequest;
use Bittacora\Bpanel4Users\Http\Requests\UpdateUserRequest;
use Bittacora\Bpanel4Users\Models\User;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Spatie\Permission\Models\Role;

class UserController extends Controller
{
    /**
     * @return mixed
     */
    public function index(){
        $this->authorize('user.index');
        return view('user::index');
    }

    /**
     * @return View
     */
    public function create(){
        $this->authorize('user.create');
        $roles = Role::all()->pluck('name', 'id')->toArray();
        return view('user::create', compact('roles'));
    }

    /**
     * @param StoreUserRequest $request
     */
    public function store(StoreUserRequest $request){
        $this->authorize('user.create');
        $user = User::create($request->validated());
        $user->syncRoles($request->input('roles'));

        Session::put('alert-success', 'El usuario ha sido dado de alta correctamente');
        return redirect(route('user.index'));
    }

    /**
     * @param User $user
     * @return Application|Factory|View
     * @throws AuthorizationException
     */
    public function show(User $user){
        $this->authorize('user.show');
        return view("user::show", compact('user'));
    }

    /**
     * @param User $user
     * @return Application|Factory|View
     * @throws AuthorizationException
     */

    public function edit(User $user){
        $this->authorize('user.edit');
        $roles = Role::all()->pluck('name', 'id')->toArray();
        $userRoles = $user->roles()->pluck('id')->toArray();
        return view("user::edit", compact('user', 'roles', 'userRoles'));
    }

    /**
     * @param UpdateUserRequest $request
     * @param User $user
     * @throws AuthorizationException
     */
    public function update(UpdateUserRequest $request, User $user){
        $this->authorize('user.edit');
        $requestData = $request->validated();

        if (!empty($requestData['password'])) {
            $requestData['password'] = Hash::make($requestData['password']);
        } else {
            unset($requestData['password']);
        }

        $user->update($requestData);
        $user->syncRoles($request->input('roles'));
        Session::put('alert-success', 'El usuario ha sido editado correctamente');
        return redirect(route('user.edit', $user));
    }

    /**
     * @param User $user
     */
    public function destroy(User $user){
        $this->authorize('user.destroy');
        $user->delete();

        Session::put('alert-success', 'El usuario ha sido dado de baja correctamente');
        return redirect(route('user.index'));
    }
}
