<?php

namespace Bittacora\Bpanel4Users\Http\Livewire;

use App\Models\User;
use Livewire\Component;

class DatatableCheckbox extends Component
{
    public $name;
    public $idField;
    public $value = null;
    public $checked = false;
    public $multiple = false;
    public $title = null;
    public $labelText;
    public $user;

    public function mount(User $user){
        $this->user = $user;
    }

    public function render()
    {
        return view('user::livewire.datatable-checkbox')->with([
            'name' => $this->name,
            'idField' => $this->idField,
            'value' => $this->checked,
            'checked' => $this->checked,
            'multiple' => $this->multiple,
            'labelText' => $this->labelText,
        ]);
    }

    public function toggle(User $user){
        $user->active = ($user->active==1) ? 0 : 1;
        $user->save();
    }
}
