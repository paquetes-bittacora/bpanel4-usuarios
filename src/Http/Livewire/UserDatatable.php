<?php

namespace Bittacora\Bpanel4Users\Http\Livewire;

use App\Models\User;
use Illuminate\Database\Eloquent\Builder;
use Rappasoft\LaravelLivewireTables\DataTableComponent;
use Rappasoft\LaravelLivewireTables\Views\Column;

class UserDatatable extends DataTableComponent
{

    public function columns(): array
    {
        return [
            Column::make('Nombre', 'name')->searchable(fn(Builder $query, $searchTerm) => $query
                ->whereRaw('LOWER(`name`) LIKE ?', [addslashes(strtolower('%' . trim(json_encode($searchTerm), '"'))) . '%'])
            ),
            Column::make('Correo electrónico', 'email')->searchable(fn(Builder $query, $searchTerm) => $query
                ->orWhereRaw('LOWER(`email`) LIKE ?', [strtolower('%' . trim($searchTerm) . '%')])
            ),
            Column::make('Fecha de alta', 'created_at'),
            Column::make('Activo', 'active')->view('user::livewire.datatable-columns.active'),
            Column::make('Acciones', 'id')->view('user::livewire.datatable-columns.actions'),
        ];
    }

    public function query(): Builder
    {
        return User::query()->orderBy('created_at', 'DESC')->
        when($this->getAppliedFilterWithValue('search'), fn ($query, $term) => $query->where('name', 'like', '%'.strtoupper($term).'%')
            ->orWhere('name', 'like', '%'.strtolower($term).'%')->orWhere('name', 'like', '%'.ucfirst($term).'%')->orWhere('email','like','%'.$term.'%'));
    }

    public function rowView(): string
    {
        return 'user::livewire.user-datatable';
    }

    public function bulkActions(): array
    {
        return [
            'bulkDelete' => 'Eliminar'
        ];
    }

    public function bulkDelete(){
        if(count($this->selectedKeys())){
            User::destroy($this->selectedKeys);
            $this->resetAll();
        }
    }

    public function configure(): void
    {
        $this->setPrimaryKey('id');
    }

    public function builder(): Builder
    {
        return $this->query();
    }
}
