<?php

namespace Bittacora\Bpanel4Users\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Str;

class StoreUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|unique:users|max:255',
            'email' => 'required|unique:users|max:255|email:rfc',
            'password' => 'required|min:8|max:16|same:repeatPassword',
            'active' => 'required',
            'rememberToken' => 'min:20'
        ];
    }

    public function attributes(){
        return[
            'email' => __('user::validation.email_attribute'),
            'name' => __('user::validation.name_attribute'),
            'password' => __('user::validation.password_attribute'),
            'repeatPassword' => __('user::validation.repeat_password_attribute')
        ];
    }

    public function messages(){
        return[
            'name.required' => __('user::validation.required_field'),
            'name.unique' => __('user::validation.name_unique_message'),
            'name.max' => __('user::validation.max_message'),
            'email.required' => __('user::validation.required_field'),
            'email.unique' => __('user::validation.email_unique_message'),
            'email.email' => __('user::validation.email_email_message'),
            'password.required' => __('user::validation.required_field'),
            'password.same' => __('user::validation.password_same_field'),
            'repeatPassword.required' => __('user::validation.required_field'),
        ];
    }

    protected function prepareForValidation()
    {
        $this->merge([
            'active' => isset($this->active),
            'remember_token' => Str::random(20)
        ]);
    }

}
