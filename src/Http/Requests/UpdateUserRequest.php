<?php

namespace Bittacora\Bpanel4Users\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Hash;

class UpdateUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'name' => 'required|max:255|unique:users,name,'.$this->user->id,
            'email' => 'required|max:255|email:rfc|unique:users,email,'.$this->user->id,
            'active' => 'required',
            'password' => 'nullable|same:repeatPassword',
        ];

        if($this->has('changePassword')){
            $rules['password'] = 'required|same:repeatPassword|min:8|max:16';
        }

        return $rules;

    }

    public function messages(){
        return[
            'name.required' => __('user::validation.required_field'),
            'name.unique' => __('user::validation.name_unique_message'),
            'name.max' => __('user::validation.max_message'),
            'email.required' => __('user::validation.required_field'),
            'email.unique' => __('user::validation.email_unique_message'),
            'email.email' => __('user::validation.email_email_message'),
            'password.required' => __('user::validation.required_field'),
            'password.same' => __('user::validation.password_same_field'),
            'repeatPassword.required' => __('user::validation.required_field')
        ];
    }


    /**
     * Handle a passed validation attempt.
     *
     * @return void
     */
    protected function passedValidation()
    {
        if ($this->has('changePassword')) {
            $this->merge(
                ['password' => $this->input('password')]
            );
        }
    }

    public function validated($key = null, $default = null)
    {
        if ($this->has('changePassword')) {
            return array_merge(parent::validated(), ['password' => $this->input('password')]);
        }
        return parent::validated();
    }

    public function attributes(){
        return[
            'email' => __('user::validation.email_attribute'),
            'name' => __('user::validation.name_attribute'),
            'password' => __('user::validation.password_attribute'),
            'repeatPassword' => __('user::validation.repeat_password_attribute')
        ];
    }

    protected function prepareForValidation()
    {
        $this->merge([
            'active' => isset($this->active),
        ]);
    }
}
