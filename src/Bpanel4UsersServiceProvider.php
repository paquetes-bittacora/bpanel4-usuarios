<?php

namespace Bittacora\Bpanel4Users;

use Bittacora\Bpanel4Users\Commands\Bpanel4UsersInstallCommand;
use Bittacora\Bpanel4Users\Commands\RegisterInactiveUserMiddleware;
use Bittacora\Bpanel4Users\Commands\UserCommand;
use Bittacora\Bpanel4Users\Contracts\UsersModule;
use Bittacora\Bpanel4Users\Http\Livewire\DatatableCheckbox;
use Bittacora\Bpanel4Users\Http\Livewire\UserDatatable;
use Bittacora\Bpanel4Users\Models\User;
use Bittacora\Bpanel4Users\Observers\UserObserver;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Hash;
use Laravel\Fortify\Fortify;
use Livewire;
use Spatie\LaravelPackageTools\Package;
use Spatie\LaravelPackageTools\PackageServiceProvider;

class Bpanel4UsersServiceProvider extends PackageServiceProvider
{
    public function configurePackage(Package $package): void
    {
        $package
            ->name('bpanel4-users')
            ->hasConfigFile()
            ->hasViews()
            ->hasMigration('create_bpanel4-users_table')
            ->hasCommand(Bpanel4UsersInstallCommand::class);
    }

    public function boot(): void
    {
        // Cambio el modelo global para los usuarios al nuestro.
        Config::set('auth.providers.users.model', User::class);
        $this->commands([Bpanel4UsersInstallCommand::class, RegisterInactiveUserMiddleware::class]);
        User::observe(UserObserver::class);
        $this->loadMigrationsFrom(__DIR__.'/../database/migrations');
        $this->loadRoutesFrom(__DIR__.'/../routes/web.php');
        $this->loadViewsFrom(__DIR__. '/../resources/views', 'user');
        $this->loadTranslationsFrom(__DIR__ .'/../resources/lang', 'user');
        Livewire::component('datatable-checkbox', DatatableCheckbox::class);
        Livewire::component('user::user-datatable', UserDataTable::class);
        $this->app->bind(UsersModule::class, Bpanel4Users::class);

        $this->registerFortifyViews();
    }

    private function registerFortifyViews(): void
    {
        Fortify::loginView(function () {
            return view('bpanel4::auth.login');
        });

        Fortify::authenticateUsing(function (Request $request) {
            $user = User::where('email', $request->email)->first();

            if ($user &&
                Hash::check($request->password, $user->password)) {
                return $user;
            }
        });

        Fortify::registerView(function () {
            return view('auth.register');
        });

        Fortify::requestPasswordResetLinkView(function () {
            return view('auth.forgot-password');
        });

        Fortify::resetPasswordView(function ($request) {
            return view('auth.reset-password', ['request' => $request]);
        });

        Fortify::verifyEmailView(function () {
            return view('auth.verify-email');
        });
    }

    private function setUserClass()
    {
        config(['auth.providers.users.model' => User::class]);
    }
}
