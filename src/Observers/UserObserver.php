<?php

namespace Bittacora\Bpanel4Users\Observers;

use App\Models\User;
use Illuminate\Support\Facades\Hash;

class UserObserver
{
    public function creating(User $user){
        // En algunas versiones de Laravel el hash se hace automáticamente y en otras no, para que el paquete
        // tenga esto en cuenta y no hashee algo que ya está hasheado, compruebo primero si la contraseña está
        // hasheada previamente.
        if (!Hash::isHashed($user->password)) {
            $user->password = Hash::make($user->password);
        }

        $user->personal_token = base64_encode(json_encode(time()));

        return $user;
    }

    public function updating(User $user){
        //$user->password = Hash::make($user->password);
        return $user;
    }

    /**
     * Handle the User "created" event.
     *
     * @param User $user
     * @return void
     */
    public function created(User $user)
    {
        //
    }

    /**
     * Handle the User "updated" event.
     *
     * @param User $user
     * @return void
     */
    public function updated(User $user)
    {
        //
    }

    /**
     * Handle the User "deleted" event.
     *
     * @param User $user
     * @return void
     */
    public function deleted(User $user)
    {
        //
    }

    /**
     * Handle the User "restored" event.
     *
     * @param User $user
     * @return void
     */
    public function restored(User $user)
    {
        //
    }

    /**
     * Handle the User "force deleted" event.
     *
     * @param User $user
     * @return void
     */
    public function forceDeleted(User $user)
    {
        //
    }
}
