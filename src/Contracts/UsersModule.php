<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4Users\Contracts;

interface UsersModule
{
    public function getNameById(int $id): string;
}