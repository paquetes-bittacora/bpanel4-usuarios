<?php

namespace Bittacora\Bpanel4Users\Commands;

use Bittacora\Bpanel4Users\Database\Seeders\DatabaseSeeder;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Artisan;

class Bpanel4UsersInstallCommand extends Command
{
    public $signature = 'bpanel4-users:install';

    public $description = 'Instala el módulo de usuarios';

    public function handle()
    {
        $this->info('Ejecutando las migraciones...');
        Artisan::call('db:seed', [
            '--class' => DatabaseSeeder::class,
            '--force' => true
        ]);
    }
}
