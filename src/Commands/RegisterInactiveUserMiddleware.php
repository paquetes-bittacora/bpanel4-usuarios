<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4Users\Commands;

use Illuminate\Console\Command;

final class RegisterInactiveUserMiddleware extends Command
{
    protected $signature = 'bpanel4:register-inactive-user-middleware';

    public function handle(): void
    {
        $filePath = app_path('Http/Kernel.php');
        $content = file_get_contents($filePath);
        $class = '\Bittacora\Bpanel4Users\Middleware\LogoutInactiveUserMiddleware::class';

        if (str_contains($content, $class)) {
            return;
        }

        $content = preg_replace(
            '/(\'web\' => \[)(.*?)(\])/s',
            "$1$2    $class\n        $3",
            $content
        );

        file_put_contents($filePath, $content);
    }

}