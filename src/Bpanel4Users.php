<?php

namespace Bittacora\Bpanel4Users;

use App\Models\User;
use Bittacora\Bpanel4Users\Contracts\UsersModule;

class Bpanel4Users implements UsersModule
{
    public function getActive()
    {
        return User::active()->get();
    }

    public function getNameById(int $id): string
    {
        $user = User::where('id', $id)->first();

        return $user->name;
    }

    public function getAllAsArray()
    {
        $users = User::orderBy('name', 'DESC')->pluck('name', 'id')->toArray();
        return $users;
    }
}
