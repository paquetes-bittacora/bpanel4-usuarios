<?php

namespace Bittacora\Bpanel4Users;

use Illuminate\Support\Facades\Facade;

/**
 * @see \Bittacora\Bpanel4Users\Bpanel4Users
 */
class Bpanel4UsersFacade extends Facade
{
    protected static function getFacadeAccessor()
    {
        return Bpanel4Users::class;
    }
}
