# Usuarios

Paquete para gestionar usuarios, login, etc

Hay que ejecutar bpanel4-users:install para ejecutar los seeders

## 🔀 Cambiar ruta a la que se redirige después del login

Este paquete usa Laravel Fortify, y para modificar algunas de sus opciones habrá
que publicar primero los archivos de configuración:

```bash
php artisan vendor:publish --provider="Laravel\Fortify\FortifyServiceProvider"
```

Una vez publicados, para cambiar la ruta a la que se redirigirá al usuario
después de identificarse, habrá que ir a `config/fortify.php` y cambiar el
parámetro `home`, que por defecto viene configurado como:  

```php
'home' => RouteServiceProvider::HOME,
```

## 📑 Nota sobre las rutas

Es recomentable definir la ruta de la home del proyecto de la siguiente forma, para no tener que
redefinir la ruta a la que lleva el login de fortify. También es importante que la home tenga el 
middleware `web`. Este código podría incluirse en `routes/web.php` o donde definamos las rutas
del proyecto en concreto en el que estamos trabajando:

```php
Route::group(['middleware' => ['web']], static function () {
    /**
     * Para no tener que tocar la configuración de fortify, redirijo de /home a / . Se podría cambiar la ruta desde
     * config/fortify.php en 'home', pero prefiero que no haya que hacer cambios manualmente al instalar el paquete.
     */
    Route::get('/home', function (Illuminate\Http\Request $request) {
        $request->session()->reflash();
        return redirect('/');
    })->name('home');

    Route::get('/', [XXXPublicPagesController::class, 'index'])->name('home');
});
```

## 🔑 Contraseñas

A la hora de establecer o actualizar la contraseña del modelo `User`, hay que
tener en cuenta que no hay que pasarla hasheada, ya que el hash se crea automáticamente
desde UserObserver.

## 🧪 Ayudas para testing

La clase `\Bittacora\Bpanel4Users\Tests\Helpers\AdminHelper` incluye varios métodos para ayudar en los tests de otros módulos. Sus métodos son:

- `actingAsAdmin`: Ejecuta el test como admin (devuelve el usuario admin)
- `actingAsAdminWithPermissions`: Ejecuta el test como admin con los permisos que se pasan como array
- `createAdminRole`: Crea el rol de admin si no existe
- `createAdmin`: Crea un usuario con rol de admin
