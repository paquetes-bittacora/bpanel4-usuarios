<?php

namespace Bittacora\Bpanel4Users\Database\Seeders;

use Bittacora\Bpanel4Users\Database\Seeders\Seeds\UserSeeder;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            UserSeeder::class
        ]);
    }
}
