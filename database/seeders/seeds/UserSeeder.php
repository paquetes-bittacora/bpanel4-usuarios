<?php

namespace Bittacora\Bpanel4Users\Database\Seeders\Seeds;

use Bittacora\Bpanel4Users\Models\User;
use Bittacora\AdminMenu\AdminMenuFacade;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
use Bittacora\Tabs\Tabs;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $module = AdminMenuFacade::createModule('configuration', 'user', 'Usuarios', 'fa fa-user');
        AdminMenuFacade::createAction($module->key, 'Listar', 'index', 'fa fa-bars');
        AdminMenuFacade::createAction($module->key, 'Añadir', 'create', 'fa fa-plus');

        User::firstOrCreate([
                'name' => 'bittacora',
                'email' => 'debug@bpanel.es'
            ],
            [
                'password' => 'prueba',
            ]
        );

        $user = User::where('email', 'debug@bpanel.es')->firstOrFail();
        $user->assignRole('admin');

        $roleAdmin = Role::findByName('admin');

        $permissions = ['index', 'create', 'show', 'edit', 'destroy', 'bulkdelete'];

        foreach($permissions as $permission){
            $permission = Permission::firstOrCreate(['name' => 'user.'.$permission]);
            $roleAdmin->givePermissionTo($permission);
        }

        Tabs::createItem('user.index', 'user.index', 'user.index', 'Usuarios','fa fa-user');
        Tabs::createItem('user.index', 'user.create', 'user.create', 'Nuevo','fa fa-plus');

        Tabs::createItem('user.create', 'user.index', 'user.index', 'Usuarios','fa fa-user');
        Tabs::createItem('user.create', 'user.create', 'user.create', 'Nuevo','fa fa-plus');

//        User::factory(10)->create();
    }
}
