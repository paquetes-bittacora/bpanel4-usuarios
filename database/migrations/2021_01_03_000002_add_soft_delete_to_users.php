<?php

declare(strict_types=1);

use Bittacora\Bpanel4Users\Database\Seeders\Seeds\UserSeeder;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Schema;

return new class () extends Migration {

    private const TABLE_NAME = 'users';

    public function up(): void
    {
        Schema::table(self::TABLE_NAME, static function (Blueprint $table): void {
            $table->softDeletes();
        });

        // No llamamos a los seeders en los test porque los ralentizan
        // y no es necesario
        if (!App::runningUnitTests()) {
            Artisan::call('db:seed', [
                '--class' => UserSeeder::class,
                '--force' => true
            ]);
        }
    }

    public function down(): void
    {
        Schema::table(self::TABLE_NAME, static function (Blueprint $table): void {
            $table->dropSoftDeletes();
        });
    }
};
