<?php

namespace Bittacora\Bpanel4Users\Database\Factories;

use Bittacora\Bpanel4Users\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Spatie\Permission\Models\Permission;

/**
 * @extends Factory<User>
 * @method User createOne($attributes = [])
 */
class UserFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = User::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->name,
            'email' => $this->faker->unique()->safeEmail,
            'email_verified_at' => now(),
            'password' => Hash::make('password'),
            'remember_token' => Str::random(10),
            'active' => 1,
            'created_by' => 1,
            'updated_by' => 1
        ];
    }

    public function admin(): self
    {
        return $this->assignRole('admin');
    }

    public function withRoles(string ...$roles): self
    {
        return $this->assignRole($roles);
    }

    public function withPermissions(string ...$permissions): self
    {
        return $this->afterCreating(function (User $user) use ($permissions) {
            foreach ($permissions as $permission) {
                Permission::create(['name' => $permission]);
            }
            $user->syncPermissions($permissions);
        });
    }

    private function assignRole(...$roles): UserFactory
    {
        return $this->afterCreating(fn(User $user) => $user->syncRoles($roles));
    }

    /**
     * Indicate that the model's email address should be unverified.
     *
     * @return \Illuminate\Database\Eloquent\Factories\Factory
     */
    public function unverified()
    {
        return $this->state(function (array $attributes) {
            return [
                'email_verified_at' => null,
            ];
        });
    }
}
